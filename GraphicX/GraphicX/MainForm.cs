﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicX
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
                
        }

        private void button3_Click(object sender, EventArgs e)
        {
           FractalsForm f1 = new FractalsForm();
            //this.Hide();
            f1.Show();
           
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            TheoryForm ft = new TheoryForm();
            ft.Show();
        }
    }
}
