﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicX {
	public partial class Form1 : Form {
        
		public Form1() {
			InitializeComponent();
		}
        private int maxIterations = 20;
        private int max = 20;
        Bitmap mbitmap;
        private void button1_Click(object sender, EventArgs e)
        {
            mbitmap = new Bitmap(Output.Width, Output.Height);
            var sel = selectFormula.SelectedIndex;
			switch (sel) {
                case (0):
                    drawFractal();
                    break;
                case (1):
                    drawFractal2();
                    break;
                case 2:
                    drawFractal3();
                    break;
                default:
                    drawFractal();
                    break;
            }
            
        }

        public ComplexNumber multiplyNumbers(ComplexNumber num1, ComplexNumber num2) {
            ComplexNumber resultNum = new ComplexNumber();

            resultNum.real = num1.real * num2.real - num1.imaginary * num2.imaginary;
            resultNum.imaginary = num1.real * num2.imaginary + num1.imaginary * num2.real;

            return resultNum;
        }

        public ComplexNumber divideNumbers(ComplexNumber num1, ComplexNumber num2) {
            ComplexNumber resultNum = new ComplexNumber();

            //resultNum.real = num1.real * num2.real - num1.imaginary * num2.imaginary;
            //resultNum.imaginary = num1.real * num2.imaginary + num1.imaginary * num2.real;

            resultNum.real = ((num1.real * num2.real + num1.imaginary * num2.imaginary) /
                num2.real * num2.real + num2.imaginary * num2.imaginary);
            resultNum.imaginary = ((num2.real * num1.imaginary - num1.imaginary * num2.real) /
                num2.real * num2.real + num2.imaginary * num2.imaginary);

            return resultNum;
        }

        public ComplexNumber getSinus(ComplexNumber num) {
            ComplexNumber res = new ComplexNumber();

            res.real = Math.Sin(num.real) * Math.Cosh(num.imaginary);

            res.imaginary = Math.Cos(num.real) * Math.Sinh(num.imaginary);

            return res;
        }

        public ComplexNumber getCosinus(ComplexNumber num) {
            ComplexNumber res = new ComplexNumber();

            res.real = Math.Cos(num.real) * Math.Sinh(num.imaginary);

            res.imaginary = Math.Sin(num.real) * Math.Cosh(num.imaginary);

            return res;
        }

        public void drawFractal() {

            double zoom = 1;

            int numberOfIterations = 300;

            ComplexNumber newComplexNumber = new ComplexNumber();
            ComplexNumber oldComplexNumber = new ComplexNumber();
            ComplexNumber c = new ComplexNumber();
            c.real = -0.70176;
            c.imaginary = -0.3842;

            int w = Output.Width;
            int h = Output.Height-1;

            int i = 0;

            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    newComplexNumber.real = 4 * (x - w / 2) /
                        (0.5 * Convert.ToDouble(1) * w);
                    newComplexNumber.imaginary = 2 * (y - h / 2) /
                        (0.5 * Convert.ToDouble(1) * h);


                    

                    for (i = 0; i < maxIterations; i++) {

                        oldComplexNumber.real = newComplexNumber.real;
                        oldComplexNumber.imaginary = newComplexNumber.imaginary;

                        //newComplexNumber = oldComplexNumber.getMult(oldComplexNumber, oldComplexNumber.getSin(oldComplexNumber));
                        newComplexNumber = multiplyNumbers(oldComplexNumber, getSinus(oldComplexNumber));

                        if ((newComplexNumber.real * newComplexNumber.real + newComplexNumber.imaginary * newComplexNumber.imaginary) > max) break;
                    }

                    mbitmap.SetPixel(x, y, i < max ?
                        Color.FromArgb(i * 8 % 255, i * 20 % 255, i * 2 % 255) : Color.FromArgb(255, 255, 255));

                    Output.Image = mbitmap;

                }
            }
        }

        public void drawFractal2() {

            double zoom = 1;

            int numberOfIterations = 300;

            ComplexNumber newComplexNumber = new ComplexNumber();
            ComplexNumber oldComplexNumber = new ComplexNumber();
            ComplexNumber c = new ComplexNumber();
            c.real = -0.70176;
            c.imaginary = -0.3842;

            int w = Output.Width;
            int h = Output.Height - 1;

            int i = 0;

            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    newComplexNumber.real = 4 * (x - w / 2) /
                        (0.5 * Convert.ToDouble(1) * w);
                    newComplexNumber.imaginary = 2 * (y - h / 2) /
                        (0.5 * Convert.ToDouble(1) * h);




                    for (i = 0; i < maxIterations; i++) {

                        oldComplexNumber.real = newComplexNumber.real;
                        oldComplexNumber.imaginary = newComplexNumber.imaginary;

                        //newComplexNumber = oldComplexNumber.getMult(oldComplexNumber, oldComplexNumber.getSin(oldComplexNumber));
                        //newComplexNumber = multiplyNumbers(oldComplexNumber, getSinus(oldComplexNumber));
                        newComplexNumber = divideNumbers(getCosinus(oldComplexNumber),
                            getSinus(oldComplexNumber));

                        if ((newComplexNumber.real * newComplexNumber.real + newComplexNumber.imaginary * newComplexNumber.imaginary) > max) break;
                    }

                    mbitmap.SetPixel(x, y, i < max ?
                        Color.FromArgb(i * 8 % 255, i * 20 % 255, i * 2 % 255) : Color.FromArgb(255, 255, 255));

                    Output.Image = mbitmap;

                }
            }
        }

        public void drawFractal3() {

            double zoom = 1;

            int numberOfIterations = 300;

            ComplexNumber newComplexNumber = new ComplexNumber();
            ComplexNumber oldComplexNumber = new ComplexNumber();
            ComplexNumber c = new ComplexNumber();
            c.real = -0.70176;
            c.imaginary = -0.3842;

            int w = Output.Width;
            int h = Output.Height - 1;

            int i = 0;

            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    newComplexNumber.real = 4 * (x - w / 2) /
                        (0.5 * Convert.ToDouble(1) * w);
                    newComplexNumber.imaginary = 2 * (y - h / 2) /
                        (0.5 * Convert.ToDouble(1) * h);




                    for (i = 0; i < maxIterations; i++) {

                        oldComplexNumber.real = newComplexNumber.real;
                        oldComplexNumber.imaginary = newComplexNumber.imaginary;

                          //newComplexNumber = oldComplexNumber.getMult(oldComplexNumber, oldComplexNumber.getSin(oldComplexNumber));
                          //newComplexNumber = multiplyNumbers(oldComplexNumber, getSinus(oldComplexNumber));
                        newComplexNumber = multiplyNumbers(oldComplexNumber, multiplyNumbers(
                            getSinus(oldComplexNumber), getSinus(oldComplexNumber)));
                        if ((newComplexNumber.real * newComplexNumber.real + newComplexNumber.imaginary * newComplexNumber.imaginary) > max) break;
                    }

                    mbitmap.SetPixel(x, y, i < max ?
                        Color.FromArgb(i * 8 % 255, i * 20 % 255, i * 2 % 255) : Color.FromArgb(255, 255, 255));

                    Output.Image = mbitmap;

                }
            }
        }

        private void Form1_Load(object sender, EventArgs e) {
            selectFormula.SelectedIndex = 0;
		}

        private void selectFormula_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
