﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicX
{
    public partial class TheoryForm : Form
    {
        public TheoryForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "Це найбільша група фракталів. Одержують їх за допомогою нелінійних процесів у n-мірних просторах. Найбільш вивчені двовимірні процеси.Якщо нелінійна динамічна система володіє декількома стійкими станами, то кожний стійкий стан має деяку область початкових станів, з яких система обов'язково потрапить у розглянуті кінцеві стани. Таким чином, фазовий простір системи розбивається на області притягання аттракторів. Фарбуючи області притягання різними кольорами, можна одержати колірний фазовий портрет цієї системи (ітераційного процесу). Міняючи алгоритм вибору кольору, можна одержати складні фрактальні картини з вигадливими багатобарвними візерунками. Несподіванкою для математиків стала можливість за допомогою примітивних алгоритмів породжувати дуже складні нетривіальні структури.";
            label1.Text = "Алгебрагічні фрактали";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.ReadOnly = false;
           
            Image i = Image.FromFile("C:/Users/User/Pictures/thumb_m_11226.jpg");
            Bitmap b = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b))
            {
                g.DrawImage(i, 0, 0, 555, 230);
            }

            Clipboard.SetImage(b);
          
            richTextBox1.Paste();
            
            richTextBox1.ReadOnly = true;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
