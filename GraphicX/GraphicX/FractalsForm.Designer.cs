﻿namespace GraphicX
{
    partial class FractalsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FractalsForm));
            this.runButton = new System.Windows.Forms.Button();
            this.Output = new System.Windows.Forms.PictureBox();
            this.selectFormula = new System.Windows.Forms.ComboBox();
            this.selectColour = new System.Windows.Forms.ComboBox();
            this.selectScale = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.Output)).BeginInit();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(202)))), ((int)(((byte)(70)))));
            this.runButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(202)))), ((int)(((byte)(70)))));
            this.runButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.runButton.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.runButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.runButton.Location = new System.Drawing.Point(466, 319);
            this.runButton.Margin = new System.Windows.Forms.Padding(2);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(124, 30);
            this.runButton.TabIndex = 0;
            this.runButton.Text = "Побудувати";
            this.runButton.UseVisualStyleBackColor = false;
            this.runButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // Output
            // 
            this.Output.BackColor = System.Drawing.Color.White;
            this.Output.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Output.Location = new System.Drawing.Point(52, 11);
            this.Output.Margin = new System.Windows.Forms.Padding(2);
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(501, 291);
            this.Output.TabIndex = 1;
            this.Output.TabStop = false;
            // 
            // selectFormula
            // 
            this.selectFormula.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.selectFormula.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectFormula.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectFormula.Items.AddRange(new object[] {
            "z*sin(z)",
            "sin(z)/cos(z)",
            "z*sin(z)*sin(z)",
            "Жуліа",
            "tg(x)"});
            this.selectFormula.Location = new System.Drawing.Point(24, 319);
            this.selectFormula.Margin = new System.Windows.Forms.Padding(2);
            this.selectFormula.Name = "selectFormula";
            this.selectFormula.Size = new System.Drawing.Size(126, 32);
            this.selectFormula.TabIndex = 2;
            // 
            // selectColour
            // 
            this.selectColour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.selectColour.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectColour.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectColour.Items.AddRange(new object[] {
            "Червоний",
            "Кораловий",
            "Помаранчевий",
            "Блакитний",
            "Фіолетовий"});
            this.selectColour.Location = new System.Drawing.Point(166, 319);
            this.selectColour.Margin = new System.Windows.Forms.Padding(2);
            this.selectColour.Name = "selectColour";
            this.selectColour.Size = new System.Drawing.Size(126, 32);
            this.selectColour.TabIndex = 3;
            // 
            // selectScale
            // 
            this.selectScale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.selectScale.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectScale.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectScale.Items.AddRange(new object[] {
            "10%",
            "20%",
            "30%",
            "40%",
            "50%",
            "60%",
            "70%",
            "80%",
            "90%",
            "100%"});
            this.selectScale.Location = new System.Drawing.Point(310, 319);
            this.selectScale.Margin = new System.Windows.Forms.Padding(2);
            this.selectScale.Name = "selectScale";
            this.selectScale.Size = new System.Drawing.Size(126, 32);
            this.selectScale.TabIndex = 4;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(106, 150);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(392, 23);
            this.progressBar1.TabIndex = 6;
            this.progressBar1.Visible = false;
            // 
            // FractalsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(113)))), ((int)(((byte)(200)))));
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.selectScale);
            this.Controls.Add(this.selectColour);
            this.Controls.Add(this.selectFormula);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.runButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FractalsForm";
            this.Text = "GraphX Fractals";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Output)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.PictureBox Output;
        private System.Windows.Forms.ComboBox selectFormula;
        private System.Windows.Forms.ComboBox selectColour;
        private System.Windows.Forms.ComboBox selectScale;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}



